/**
 * Module dependencies.
 */

var express = require('express');
var MongoStore = require('connect-mongo')(express);
var flash = require('express-flash');
var path = require('path');
var mongoose = require('mongoose');
var passport = require('passport');


/**
 * API keys + Passport configuration.
 */

var secrets = require('./config/secrets');
var passportConf = require('./config/passport');

/**
 * Create Express server.
 */

var app = express();

/**
 * Mongoose configuration.
 */

mongoose.connect(secrets.db);
mongoose.connection.on('error', function() {
  console.error('✗ MongoDB Connection Error. Please make sure MongoDB is running.');
});

/**
 * Express configuration.
 */

var hour = 3600000;
var day = (hour * 24);
var week = (day * 7);
var month = (day * 30);

app.set('port', process.env.PORT || 3000);


// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'ejs');



app.use(express.compress());
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.cookieParser());
app.use(express.json());
app.use(express.urlencoded());

// app.use(expressValidator());

app.use(express.methodOverride());
app.use(express.session({
  secret: secrets.sessionSecret,
  store: new MongoStore({
    db: mongoose.connection.db,
    auto_reconnect: true
  })
}));


app.use(app.router);

app.configure(function () {

});





if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}


app.use(express.static(path.join(__dirname, 'public')));
// app.use(express.static(path.join(__dirname, 'public'), { maxAge: week }));

// app.use(function(req, res) {
//   res.status(404);
//   res.render('404');
// });


/**
 * Load controllers.
 */


var articlesController = require('./controllers/articles');


// var homeController = require('./controllers/home');
// var userController = require('./controllers/user');
// var apiController = require('./controllers/api');
// var contactController = require('./controllers/contact');


/**
 * Application routes.
 */


app.get('/', function(req, res) {

  res.redirect('/');

});


// Articles...
app.get('/articles', articlesController.findAll);
app.get('/articles/:id', articlesController.findById);
app.post('/articles', articlesController.addArticle);
app.put('/articles/:id', articlesController.updateArticle);
app.delete('/articles/:id', articlesController.deleteArticle);


/**
 * Start Express server.
 */

app.listen(app.get('port'), function() {
  console.log("✔ Express server listening on port %d in %s mode", app.get('port'), app.settings.env);
});