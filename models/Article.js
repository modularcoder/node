var mongoose = require('mongoose');

var ArticleSchema = new mongoose.Schema({

  title : {
    type: String
  },
  slug: { 
    type: String, 
    unique: true 
  },
  body: {
    type: String
  }

});


module.exports = mongoose.model('Article', ArticleSchema);
