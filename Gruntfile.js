module.exports = function ( grunt ) {

   /** 
   * Load required Grunt tasks. These are installed based on the versions listed
   * in `package.json` when you do `npm install` in this directory.
   */

   grunt.loadNpmTasks('grunt-contrib-less');
   grunt.loadNpmTasks('grunt-contrib-watch');
   grunt.loadNpmTasks('grunt-express-server');

   var userConfig = {

   };

	var taskConfig = {

		express: {
	    	options: {
	      		// Override defaults here
	    	},
	    	dev: {
	      		options: {
	        		script: 'app.js'
	      		}
	    	},
	    	prod: {
	      		options: {
	        	script: 'app.js',
	        	node_env: 'production'
	      		}
	    	},
	    	test: {
	      		options: {
	        		script: 'app.js'
	      		}
	    	}
	  	},

		less: {
			compile: {
				files: {
					'public/css/main.css': 'public/less/main.less'
				}
			},
			bootstrap: {
				files: {
					'public/css/bootstrap.css': 'public/less/bootstrap/bootstrap.less'
				}
			},
			fontAwesome: {
				files: {
					'public/css/font-awesome.css': 'public/less/font-awesome/font-awesome.less'
				}
			}
		},

		
		delta: {

	     	options: {
		        livereload: true
		    },


		    /**
	       	* When the JS files change, we need to restart server
	       	*/
		    express: {
	      		files:  [ 
	      			'app.js', 
	      			'config/**/*.js',
	      			'controllers/**/*.js',
	      			'models/**/*.js'
	      		],
	      		tasks:  [ 'express:dev' ],
	      		options: {
	        		spawn: false // for grunt-contrib-watch v0.5.0+, "nospawn: true" for lower versions. Without this option specified express won't be reloaded
	      		}
	    	},

	      	/**
	       	* When the LESS files change, we need to compile and copy to build dir
	       	*/
	      	less: {
	        	files: [ 'public/less/**/*.less' ],
	        	tasks: [ 'less:compile'],
	        	options: {
        		  livereload: true
		        }
	      	},

	      	ejs: {
	      		files: ['views/**/*.ejs'],
	      		options: {
        		  livereload: true
		        }
	      	},

	      	js: {
	      		files: ['public/js/**/*.js'],
	      		options: {
	      			livereload: true
	      		}
	      	}
	    }
	}

	grunt.initConfig( grunt.util._.extend( taskConfig, userConfig ) );
	// grunt.config.init(taskConfig);

	grunt.renameTask( 'watch', 'delta' );

  	grunt.registerTask( 'watch', [
  		'express:dev', 
  		'less:bootstrap',
  		'less:fontAwesome',
  		'less:compile',
  		'delta'
  	]);

	grunt.registerTask('default', [
		'express:dev', 
		'less:bootstrap',
		'less:fontAwesome',
  		'less:compile',
  		'delta'
	]);

}
